import torch
import os

# for angle in np.linspace(0,180,10):
#     for l in np.linspace(10,50,10):
#         cmd = f'python bias_data.py --scope_x={l} --angle={angle}'
#         # os.system(cmd)
#         print(cmd)

# beta_vs_KL
# for seed in [1,2,3]:
#     for beta in torch.linspace(1,64,50):
#         cmd = f'env python bias_data.py --scope_x=10 --angle=0 --beta={beta} --random_seed={seed}'
#         os.system(cmd)

# H_vs_KL
for seed in [1, 2, 3]:
    for l in torch.linspace(1, 40, 20):
        cmd = f'env python bias_data.py --scope_x={l} --angle=0 --beta=20 --random_seed={seed}'
        os.system(cmd)
