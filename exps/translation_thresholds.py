"""
polar : polar coordinate
angle: the orientation of the original image
"""

import argparse
from collections import defaultdict

from torch import optim
from torch.utils.data import DataLoader
import wandb
from tqdm import trange

from disvae.models.anneal import get_anneal
from disvae.models.losses import get_loss_s
from disvae.models.vae import VAE, init_specific_model
from disvae.models.encoders import get_encoder
from disvae.models.decoders import get_decoder

from utils.visualize import *
from exps.patterns import data_generator
import numpy as np
from exps.translation import train, get_preds
from utils.helpers import set_seed

# set_seed(random_seed)
img = torch.load(f'patterns/0.pat')
img = data_generator.rotate(img, 45)
dataset = data_generator.Translation(img)


def compute_threshold(action, orientation):
    params = dict(
        batch_size=20,
        epochs=2000,
        dimension=10,
        loss='betaH',
        orientation = orientation
    )
    wandb.init(project="exps", config=params, group='translation_thresholds', reinit=True)
    config = wandb.config
    # generate data

    epochs = config.epochs
    dim = config.dimension

    dl = DataLoader(action, pin_memory=True,
                    batch_size=config.batch_size, shuffle=True)

    vae = init_specific_model("Higgins", (1, 64, 64), dim, 5)
    vae.cuda()
    vae.train()

    # wandb.watch(vae, log_freq=10)
    iterations = len(dl) * epochs
    anneal = get_anneal('monotonic', iterations, 120, 1)

    loss_f = get_loss_s("betaH", anneal, 1, len(dl.dataset))

    storer = train(dl, vae, loss_f, epochs, 400, "cuda")
    vae.eval()
    kl_loss = torch.Tensor([v for k, v in storer.items() if 'kl_' == k[:3]])
    _, index = kl_loss.sort(descending=True)

    preds, targets = get_preds(vae, DataLoader(action,batch_size=100))
    data = preds.cpu()[:,index[0]]
    fig = plt.figure()
    plt.plot(data,'x')
    wandb.log({'projection':wandb.Image(fig)})

    vae.cpu()
    wandb.join()
    return storer


translation_factors = [
 (torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
  torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39])),
 (torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
  torch.tensor([10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24,
         24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30])),
 (torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
  torch.tensor([20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
         20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20])),
 (torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
  torch.tensor([30, 29, 29, 28, 28, 27, 27, 26, 26, 25, 25, 24, 24, 23, 23, 22, 22, 21, 21, 20, 20, 19, 19, 18, 18, 17, 17, 16,
         16, 15, 15, 14, 14, 13, 13, 12, 12, 11, 11, 10])),
 (torch.tensor([ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
         29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
  torch.tensor([39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12,
         11, 10,  9,  8,  7,  6,  5,  4,  3,  2,  1])),
 (torch.tensor([10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24,
         24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30]),
  torch.tensor([39, 37, 38, 35, 36, 33, 34, 31, 32, 29, 30, 27, 28, 25, 26, 23, 24, 21, 22, 19, 20, 17, 18, 15, 16, 13, 14, 11,
         12,  9, 10,  7,  8,  5,  6,  3,  4,  1,  2,  0])),
 (torch.tensor([20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
         20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20]),
  torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39])),
 (torch.tensor([10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24,
         24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30]),
  torch.tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
         28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39]))]
for s in range(5):
    for orientation,factor in enumerate(translation_factors):
        x, y = factor
        imgs = dataset.o_imgs[x, y].unsqueeze(1).clone()
        action = list(zip(imgs, torch.stack((x, y), 1)))
        storer = compute_threshold(action, orientation)
            # if storer['KL_loss'] > 0.1:
            #     break
