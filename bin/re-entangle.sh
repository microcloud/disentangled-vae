seed=1
# 4h
#python main_fraction.py -s=$seed -ss -d=dsprites -e=30 -g=5 -b=512 --loss=betaB --betaB_finC=25 --anneal_name=monotonic --anneal_l=0 --anneal_r=1
python main_fraction.py -s=$seed -ss -d=dsprites -e=30 -g=5 -b=512 --loss=betaB --betaB_finC=75 --anneal_name=monotonic --anneal_l=0 --anneal_r=1

# 2h
#python main_fraction.py -s=$seed -ss -d=chairs -e=300 -g=5 -b=512 --loss=betaB --betaB_finC=50 --anneal_name=monotonic --anneal_l=0 --anneal_r=1
python main_fraction.py -s=$seed -ss -d=chairs -e=900 -g=5 -b=512 --loss=betaB --betaB_finC=75 --anneal_name=monotonic --anneal_l=0 --anneal_r=1
