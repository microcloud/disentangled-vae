# 1h*6
seed=574
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=80 --checkpoint_every=2
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=70 --checkpoint_every=2
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=60 --checkpoint_every=2
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=50 --checkpoint_every=2
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=40 --checkpoint_every=2
python  main_fraction.py --seed=$seed -d dsprites_full -e=10 -z=10 -g=5 -ss  --beta=30 --checkpoint_every=2

for i in {0..2} ; do
  seed=$RANDOM
  # 2h
  python  main_fraction.py --seed=$seed -d dsprites_full -e=20 -z=10 -g=5 -ss --loss=betaB --beta=1 --anneal_name=monotonic --anneal_l=0 --anneal_r=1 --checkpoint_every=2 --betaB_finC=10
  # 2
  python main_fraction.py --seed=$seed -ss -d dsprites_full -e=20  -z=10 -g=5 --beta=1  --anneal_name=monotonic --anneal_l=200 --anneal_r=1 --checkpoint_every=2
done
