echo "scream_dsprites"
for seed in {1..6} ; do
  ## translation
  seed=$RANDOM
  python main_fraction.py --seed=$seed -d scream_dsprites -e=2  -z=10 -g=2 --beta=130  --lr_decay=0.1
  ## scale
  python main_fraction.py --seed=$seed -d scream_dsprites -e=15  -z=10 -g=2 --base=130 --beta=1 --lr_decay=0.1
done

seed=1
python main_fraction.py --seed=$seed -ss -d scream_dsprites -e=15  -z=10 -g=2 --beta=8 --lr_decay=0.1
python main_fraction.py --seed=$seed -ss -d scream_dsprites -e=15  -z=10 -g=2 --beta=3 --lr_decay=0.1
python main_fraction.py --seed=$seed -ss -d scream_dsprites -e=15  -z=10 -g=2 --beta=1 --lr_decay=0.1
