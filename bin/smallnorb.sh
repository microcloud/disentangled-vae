
# 4860 samples, 3950 epochs
for seed in {1..10} ; do
  seed=$RANDOM
  python main_fraction.py --seed=$seed -d smallnorb -e=20   -z=10 -g=5 --beta=30 --lr_decay=0.1
  python main_fraction.py --seed=$seed -d smallnorb -e=40   -z=10 -g=5 --base=30 --beta=5 --lr_decay=0.1
  python main_fraction.py --seed=$seed -d smallnorb -e=40   -z=10 -g=5 --base=30,5 --beta=3 --lr_decay=0.1
  python main_fraction.py --seed=$seed -d smallnorb -e=400  -z=10 -g=5 --base=30,5,3 --beta=1 --lr_decay=0.1
done
