#baseline

for seed in {111,222,333};do
python main_fraction.py --seed=$seed -d smallnorb -ss -e=500  -z=10 -g=5 --beta=6
python main_fraction.py --seed=$seed -d smallnorb -ss -e=500  -z=10 -g=5 --beta=1 --loss=betaB --anneal_name=monotonic --anneal_l=0 --anneal_r=1
python main_fraction.py --seed=$seed -d smallnorb -ss -e=500  -z=10 -g=5 --beta=1 --loss=factor --factor_G=100

python main_fraction.py --seed=$seed -d scream_dsprites -ss -e=15  -z=10 -g=5 --beta=6
python main_fraction.py --seed=$seed -d scream_dsprites -ss -e=15  -z=10 -g=5 --beta=1 --loss=betaB --anneal_name=monotonic --anneal_l=0 --anneal_r=1
python main_fraction.py --seed=$seed -d scream_dsprites -ss -e=15  -z=10 -g=5 --beta=1 --loss=factor --factor_G=100

python main_fraction.py --seed=$seed -d dsprites_full -ss -e=15  -z=10 -g=5 --beta=6
python main_fraction.py --seed=$seed -d dsprites_full -ss -e=15  -z=10 -g=5 --beta=1 --loss=betaB --anneal_name=monotonic --anneal_l=0 --anneal_r=1
python main_fraction.py --seed=$seed -d dsprites_full -ss -e=15  -z=10 -g=5 --beta=1 --loss=factor --factor_G=100
done
