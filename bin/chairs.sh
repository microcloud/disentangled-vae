#base line
#for seed in {1..3};do
# python main_fraction.py -d chairs --seed=$seed -b=128 --loss=betaH -z=15 -g=5 --betaH_B=1 -e=10 --base= --beta=60
# python main_fraction.py -d chairs --seed=$seed -b=128 --loss=betaH -z=15 -g=5 --betaH_B=1 -e=20 --base=60 --lr_decay=0.1 --beta=20
# python main_fraction.py -d chairs --seed=$seed -b=128 --loss=betaH -z=15 -g=5 --betaH_B=1 -e=200 --base=60,20 --lr_decay=0.1 --beta=4
#done
#python main_fraction.py -d chairs -b=128 --loss=betaH -z=16 -g=4 --betaH_B=1 -e=10 --base= --beta=60
#python main_fraction.py -d chairs -b=128 --loss=betaH -z=16 -g=4 --betaH_B=1 -e=20 --base=60 --beta=20

python main_fraction.py -d chairs -b=128 --loss=betaH -z=16 -g=4 --betaH_B=1 -e=20 --base=60,20 --lr_decay=0.1 --beta=10
python main_fraction.py -d chairs -b=128 --loss=betaH -z=16 -g=4 --betaH_B=1 -e=200 --base=60,20,10 --lr_decay=0.1 --beta=4
python main_fraction.py -d chairs -b=128 --loss=betaH -z=16 -g=4 --betaH_B=1 -e=200 --base=60,20,10 --lr_decay=0.1 --beta=2