#!/usr/bin/env python
import os,sys
seed = 148 # 用来标记来源
ds='dsprites'

for epoches in [2,4,8]:
    for sep in ['100 70 50 20 1',
                '70 50 20 1',
                '50 20 1']:
        try:
            r=os.system(f'python main.py -g=5 '
                        f'--loss=betaH '
                        f'-s={seed} -d={ds} -e={epoches} -b=512 '
                        f' --anneal_name=constant --sep {sep} ')
            if r!=0:
                exit(r)

        except KeyboardInterrupt:
            exit(-1)