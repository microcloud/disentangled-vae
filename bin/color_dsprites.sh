echo "color_dsprites"
for seed in {1..6} ; do
  ## translation
  seed=$RANDOM
  python main_fraction.py --seed=$seed -d color_dsprites -e=2  -z=10 -g=5 --beta=160 --lr_decay=0.1
  ## scale
  python main_fraction.py --seed=$seed -d color_dsprites -e=2  -z=10 -g=5 --base=160 --beta=130 --lr_decay=0.1
  ## shape
  python main_fraction.py --seed=$seed -d color_dsprites -e=4  -z=10 -g=5 --base=160,130 --beta=30 --lr_decay=0.1
  ## rotation
  python main_fraction.py --seed=$seed -d color_dsprites -e=15 -z=10 -g=5  --base=160,130,30  --beta=1 --lr_decay=0.1
done

# factor
for seed in {1..3} ; do
  ## translation
  python main_fraction.py --seed=$seed -d color_dsprites -e=2  -z=10 -g=5 --beta=1 --lr_decay=0.1 --loss=factor --anneal_r=160
  ## scale
  python main_fraction.py --seed=$seed -d color_dsprites -e=2  -z=10 -g=5 --base=1 --beta=130 --lr_decay=0.1 --loss=factor --anneal_r=130
  ## shape
  python main_fraction.py --seed=$seed -d color_dsprites -e=4  -z=10 -g=5 --base=1,1 --beta=30 --lr_decay=0.1 --loss=factor --anneal_r=30
  ## rotation
  python main_fraction.py --seed=$seed -d color_dsprites -e=15 -z=10 -g=5  --base=1,1,1  --beta=1 --lr_decay=0.1 --loss=factor --anneal_r=1
done