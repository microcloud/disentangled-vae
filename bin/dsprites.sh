for seed in {1..6} ; do
  ## translation
  seed=$RANDOM
  python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --beta=70 --lr_decay=0.1
  ## scale
  python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --base=70 --beta=30 --lr_decay=0.1
  ## shape
  python main_fraction.py --seed=$seed -d dsprites_full -e=4  -z=10 -g=5 --base=70,30 --beta=12 --lr_decay=0.1
  ## rotation
  python main_fraction.py --seed=$seed -d dsprites_full -e=15 -z=10 -g=5  --base=70,30,12  --beta=1 --lr_decay=0.1
done
# anneal 对比

# 证明fractional的必要性
seed=741
python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --beta=70 --lr_decay=0.1
for i in {0,0.1,0.3,0.5,0.7,1} ; do
python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --base=70 --beta=1 --lr_decay=$i
done

#for seed in {401..403} ; do
#python main_fraction.py --seed=$seed -d dsprites_full -e=20 -z=10 -g=5  -ss --loss=betaB --beta=1 --anneal_name=monotonic --anneal_l=0 --anneal_r=1 --checkpoint_every=2
#done

# 拓展实验：与factorVAE的结合
#rand=(548 562 145)
for i in {0..2} ; do
  ## translation
  seed=${rand[i]}
  python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --beta=70 --lr_decay=0.1 --loss=factor --anneal_r=70
  ## scale
  python main_fraction.py --seed=$seed -d dsprites_full -e=2  -z=10 -g=5 --base=70 --beta=30 --lr_decay=0.1 --loss=factor --anneal_r=30
  ## shape
  python main_fraction.py --seed=$seed -d dsprites_full -e=4  -z=10 -g=5 --base=70,30 --beta=12 --lr_decay=0.1 --loss=factor --anneal_r=12
  ## rotation
  python main_fraction.py --seed=$seed -d dsprites_full -e=15 -z=10 -g=5  --base=70,30,12  --beta=1 --lr_decay=0.1 --loss=factor --anneal_r=1
done

