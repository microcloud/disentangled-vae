#!/usr/bin/env bash

for seed in {1..3} ; do
  # 4h
    python main_fraction.py -s=$seed -ss -d=dsprites -e=50 -g=5 -b=512 --loss=betaH --betaH_B=4
    python main_fraction.py -s=$seed -ss -d=dsprites -e=50 -g=5 -b=512 --loss=betaB --anneal_name=monotonic --anneal_l=0 --anneal_r=1
done

for seed in {1..3} ; do
  # 2h
    python main_fraction.py -s=$seed -ss -d=chairs -e=300 -g=5 -b=512 --loss=betaH --betaH_B=4
    python main_fraction.py -s=$seed -ss -d=chairs -e=300 -g=5 -b=512 --loss=betaB --anneal_name=monotonic --anneal_l=0 --anneal_r=1
done