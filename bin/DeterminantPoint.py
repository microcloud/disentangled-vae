#!/usr/bin/env python
import os,sys
seed = 458 # 用来标记来源
ds = 'dsprites'
for epoches in [50]:
    for loss in ['betaH', ]:
        if loss =='betaH':
            search_list = [ 1 / 4, 1 / 2, 2, 4 ,12]
        else:
            search_list =[1, 20, 30, 40, 60, 80]

        for strength in search_list:
            try:
                r=os.system(f'python main.py '
                            f'--loss={loss} '
                            f'-s={seed} -d={ds} -e={epoches} -b=512 '
                            f' --anneal_name=constant --anneal_l={strength} --anneal_r={strength} ')
                if r!=0:
                    exit(r)

            except KeyboardInterrupt:
                print(2)
                exit(-1)
